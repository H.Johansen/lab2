package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge { //Når vi implementerer interfacet så må vi bruke alle metodene fra IFridge
    //Trykk autoimplement 
    int max_Capacity=20;
    ArrayList <FridgeItem> items = new ArrayList<>();
    //Må ikke legge til objektet string som typen men objektet Fridgeitem som er dannet i den andre filen
    @Override
    public int totalSize(){
        return max_Capacity;
    }

    @Override //må ikke ha med override, men er greit for de som skal lese koden, om at koden kommer fra et interface
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item){
        // TODO Auto-generated method stub
        if (items.size()>=max_Capacity){
            return false;
        }
        return items.add(item);
    }
        
    

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (!items.contains(item)){ //Stort sett bedre å sjekke om items ikke er i listen enn om det er i
            throw new NoSuchElementException("No such item in fridge.");
        }
        items.remove(item);    
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List <FridgeItem> expiredFood=new ArrayList<FridgeItem>();
        for (FridgeItem food:items){
            if (food.hasExpired()){
                expiredFood.add(food);
            }
        }
        for (FridgeItem expFood:expiredFood){
            items.remove(expFood); //Kan ikke fjerne ting mens vi iterer så må ha to for loops
        }
        return expiredFood;
    
    }
}
